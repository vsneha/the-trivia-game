NAME: The name of the Game is The-Trivia-Game
DATABASE:  https://noroff-assigment-api.herokuapp.com/trivia
HEROKU LINK: https://rocky-anchorage-19651.herokuapp.com/

DESCRIPTION:
• The Game start when the user types his NAME and selects the choices of 
            Number of questions 
            several catogeries and
            difficulty
by the user.
• The user gets to answer the questions depending on the selection
• The user has the option to REPLAY or RESTART the game once he has completed it.
• He gets the total SCORE for this game along with the correct and wrong answers.


Visuals
• A COmponent tree is used to show the pages and the features of the component.


ISTALLATION and CONTRIBUTIONS: Its a VUEX project.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

USAGE:
• This guide is used to create the project https://vuejs.org/v2/guide/

SUPPORT:
The deployment help: https://gitlab.com/javascript-project-examples/heroku-deployment-guides 
Rest API: https://github.com/dewald-els/noroff-assignment-api

AUTHORS and ACKNOWLEDGMENT: 

I have worked alone on the project

LICENSE:
For open source projects, say how it is licensed.

PROJECT STATUS:
Deployed to Heroku
