import Vue from "vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
// Importing Bootstrap an BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
//Importing styles
import "./assets/style.css";
import "animate.css";
//riuters and app are rendered here
import router from "./router";
import App from "./App.vue";
import store from "./store";

// Making BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
