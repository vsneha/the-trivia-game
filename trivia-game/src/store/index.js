import Vue from "vue";
//import { set } from "vue/types/umd";
import Vuex from "vuex";
import { TriviaAPI } from "../components/Trivia/TriviaAPI";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    username: null,
    difficulty: null,
    //states takes an string value to make it as an interger so defining as NAN
    numberOfQuestions: NaN,
    category: 0,
    listOfCategories: [],
    userList: [],
    listOfQuestions: [],
    currentQuestionId: 0,
    selectedAnswer: [],
    error: "",
  },
  // mutations are normally used to change the state variable
  //payload is where you change the state of the variable (only once)
  mutations: {
    setUsername: (state, payload) => {
      state.username = payload;
    },
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setNumberOfQuestions: (state, payload) => {
      state.numberOfQuestions = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;
    },
    setListOfCategories: (state, payload) => {
      state.listOfCategories = payload;
    },
    setListOfQuestions: (state, payload) => {
      state.listOfQuestions = payload;
    },
    setError: (state, payload) => {
      state.error = payload;
    },
    // dispalying one question at a time on the page
    incrementCurrentQuestionId: (state) => {
      state.currentQuestionId += 1;
      console.log(state.currentQuestionId);
    },
    // adding list of answers
    setSelectedAnswer: (state, payload) => {
      if (state.currentQuestionId >= state.selectedAnswer.length) {
        state.selectedAnswer.push(payload);
      } else {
        state.selectedAnswer[state.currentQuestionId] = payload;
      }
      console.log(state.selectedAnswer);
    },
    //reseting the state
    reset: (state) => {
      state.listOfQuestions = [];
      state.currentQuestionId = 0;
      state.selectedAnswer = [];
      state.numberOfQuestions = NaN;
      state.difficulty = null;
      state.category = 0;
    },
    //replay the game with same category and difficulty
    resetQuestion: (state) => {
      state.listOfQuestions = [];
      state.currentQuestionId = 0;
      state.selectedAnswer = [];
    },
  },
  // getters are used to get the value from the state to be used in any componenets
  getters: {
    getUsername: (state) => {
      return state.username;
    },
    getDifficulty: (state) => {
      return state.difficulty;
    },
    getCategory: (state) => {
      return state.category;
    },
    getListOfQuestions: (state) => {
      return state.listOfQuestions;
    },
    getUserList: (state) => {
      return state.userList;
    },
    getCategories: (state) => {
      return state.listOfCategories;
    },
    getNumberOfQuestions: (state) => {
      return state.numberOfQuestions;
    },

    // one question displayed on a Page
    getListOfQuestionsDisplay: (state) => {
      let listOfQuestions = state.listOfQuestions;
      let i = state.currentQuestionId;
      console.log(listOfQuestions);
      if (i < listOfQuestions.length) {
        //adding the list of incorrect answers
        let answerArray = [...listOfQuestions[i].incorrect_answers];
        //adding the correct answer
        let correctAnswer = listOfQuestions[i].correct_answer;
        answerArray.push(correctAnswer);
        //sorting the incorrect answer array
        let incorrect_answer = answerArray.sort((a, b) => a.localeCompare(b));
        console.log(incorrect_answer);
        return {
          id: i,
          question: listOfQuestions[i].question,
          correct_answer: listOfQuestions[i].correct_answer,
          incorrectAnswerArray: incorrect_answer,
        };
        //When the question is the last question then it goes to result page
      } else {
        return {
          id: i,
          question: "",
          correct_answer: "",
          incorrectAnswerArray: [],
        };
      }
    },
    //gameCompleted = done with all questions and redirect them to results page
    getIncorrectAnswerArray: (state, getters) => {
      const listOfQuestions = state.listOfQuestions;
      console.log(listOfQuestions);
      if (listOfQuestions.length == 0 || getters.gameCompleted) {
        return [];
      }
      console.log(getters.getListOfQuestionsDisplay.incorrectAnswerArray);
      return getters.getListOfQuestionsDisplay.incorrectAnswerArray;
    },
    //getting result in results page
    getResults: (state) => {
      let results = [];
      for (let i = 0; state.listOfQuestions.length > i; i++) {
        let question = state.listOfQuestions[i].question;
        let correctAnswer = state.listOfQuestions[i].correct_answer;
        let selectedAnswer = state.selectedAnswer[i];
        let result = {
          id: i,
          question: question,
          correctAnswer: correctAnswer,
          selectedAnswer: selectedAnswer,
        };
        results.push(result);
      }
      return results;
    },
    // getting the score of the questions that are answered
    getScore: (state, getters) => {
      let score = 0;
      for (const result of getters.getResults) {
        if (result.correctAnswer === result.selectedAnswer) {
          score++;
          console.log(
            score,
            result.correctAnswer,
            result.selectedAnswer,
            result
          );
        }
      }
      return {
        score: score,
        total: state.listOfQuestions.length,
      };
    },
    // if we are not left with any questions
    gameCompleted: (state) => {
      return state.listOfQuestions.length <= state.currentQuestionId;
    },
    isQuestionAnswered: (state) => {
      return state.currentQuestionId < state.selectedAnswer.length;
    },
  },
  // registering the username and updating if the score is higher
  actions: {
    async registerUser({ state, commit }) {
      try {
        const userList = await TriviaAPI.getAllUser();
        const { username } = state;
        const userByName = userList.filter((n) => n.username === username);
        //console.log("userByName", username, userByName);
        if (userByName.length > 0) {
          const registerDetails = JSON.stringify({
            id: userByName[0].id,
            username: state.username,
            highScore:
              userByName[0].highScore > this.getters.getScore.score * 10
                ? userByName[0].highScore
                : this.getters.getScore.score * 10,
          });
          const user = await TriviaAPI.register(registerDetails, "PATCH");
          if (user) {
            commit("setUser", user);
          } else {
            commit("setError", "user cannot be registered");
          }
        } else {
          const registerDetails = JSON.stringify({
            username: state.username,
            highScore: this.getters.getScore.score * 10,
          });
          const user = await TriviaAPI.register(registerDetails, "POST");
          if (user) {
            commit("setUser", user);
          } else {
            commit("setError", "user cannot be registered");
          }
        }
      } catch (error) {
        commit("setError", error.message);
      }
    },
    async fetchUser({ commit }) {
      try {
        const userList = await TriviaAPI.getAllUser();
        commit("setUserList", userList);
      } catch (error) {
        commit("setError", error.message);
      }
    },
    async fetchListOfCategories({ commit }) {
      try {
        const listOfCategories = await TriviaAPI.fetchListOfCategories();
        console.log(listOfCategories);
        commit("setListOfCategories", listOfCategories);
      } catch (error) {
        commit("setError", error.message);
      }
    },
    async fetchListOfQuestions({ commit, state }) {
      let { numberOfQuestions } = state;
      const { difficulty } = state;
      const { category } = state;
      console.log(numberOfQuestions, difficulty, category);
      let API_suffix = "";

      if (numberOfQuestions == numberOfQuestions && numberOfQuestions != 0) {
        API_suffix += `amount=${numberOfQuestions}`;
      } else {
        numberOfQuestions = 10;
        API_suffix += `amount=${numberOfQuestions}`;
      }

      if (difficulty) {
        API_suffix += `&difficulty=${difficulty}`;
      }

      if (category != 0) {
        API_suffix += `&category=${category}`;
      }
      API_suffix += "&type=multiple";
      try {
        const trivia = await TriviaAPI.fetchListOfQuestions(API_suffix);
        commit("setListOfQuestions", trivia);
        console.log(trivia.length);
      } catch (error) {
        commit("setError", error.message);
      }
    },
  },
});
