import { constants } from "../constants";

export const TriviaAPI = {
  getAllUser() {
    return fetch(
      "https://noroff-assigment-api.herokuapp.com/trivia"
    ).then((response) => response.json().then((data) => data));
  },
  register(registerDetails, method) {
    const requestOptions = {
      method: method,
      headers: {
        "content-Type": "application/json",
        "x-api-key": "assessment",
      },
      body: registerDetails,
    };
    const { id } = JSON.parse(registerDetails);

    console.log(id, registerDetails);
    return fetch(
      method == "PATCH" ? "https://noroff-assigment-api.herokuapp.com/trivia/"+id : "https://noroff-assigment-api.herokuapp.com/trivia",
      requestOptions
    )
      .then((response) => response.jsonP())
      .then((data) => data.data);
  },

  fetchListOfQuestions(API_suffix) {
    return fetch(constants.API_TRIVIA_ROOT + API_suffix).then((response) =>
      response.json().then((data) => data.results)
    );
  },

  fetchListOfCategories() {
    return fetch("https://opentdb.com/api_category.php")
      .then((response) => response.json())
      .then((data) => data.trivia_categories);
  },
};
