import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "StartScreen",
    component: () => import("../components/Trivia/StartScreen.vue"),
  },
  {
    path: "/questions",
    name: "QuestionScreen",
    component: () => import("../components/Trivia/QuestionScreen.vue"),
  },
  {
    path: "/results",
    name: "ResultScreen",
    component: () => import("../components/Trivia/ResultScreen.vue"),
  },
];
const router = new VueRouter({
  //navigation saves the history of the previous page
  mode: "history",
  //automatically takes the URL of the server  ex: localhost 8008
  base: process.env.BASE_URL,
  routes,
});
export default router;
